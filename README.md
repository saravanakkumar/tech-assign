# Initial setup:-
1. Install virutalbox
2. Install vagrant
3. verify if any base image exist
   vagrant box list 
4. Download the source code
   git clone git@gitlab.com:saravanakkumar/tech-assign.git;cd tech-assign
5. Install ansible
6. Install nginx ansible pre-requisites
   ansible-galaxy install nginxinc.nginx

# Start the service:-
Below step will boot the ubuntu and install python, java8, nginx
```
vagrant up.
```

# To connect the instance
```
vagrant ssh
```

After this you can access verify the service status as below.
- To check airport-service v1.0.1: http://127.0.0.1:8080/airports/NL
- To check country-service : http://127.0.0.1:9090/countries

# Access service via LoadBalancer
* http://127.0.0.1:8000/airports/NL
* http://127.0.0.1:8000/countries

# For zero downtime deployment (airport-service) - CI/CD:
To achieve zero downtime playbook will deploy the new jar in different port without affecting the existing version, wait for service healthcheck to pass. After the health check passed, it will update the nginx configuration with the new port and reload the service.
```
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory deploy/playbook.yml
```
# For directly accessing the airport-service v1.1.0 version
* http://127.0.0.1:8080/airports/EHAM
* http://127.0.0.1:8080/search/NL

# Access service via LoadBalancer
* http://127.0.0.1:8000/airports/EHAM
* http://127.0.0.1:8000/search/NL
* http://127.0.0.1:8000/countries

# To remove the old service 
```
ansible-playbook -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory deploy/rm_old_svc.yml
```